"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("uuid");
var CurrencyEnum;
(function (CurrencyEnum) {
    CurrencyEnum["USD"] = "USD";
    CurrencyEnum["UAH"] = "UAH";
})(CurrencyEnum || (CurrencyEnum = {}));
class Transaction {
    constructor(id = (0, uuid_1.v4)(), amount, currency) {
        this.id = id;
        this.amount = amount;
        this.currency = currency;
    }
}
class Card {
    constructor() {
        this.transactions = [];
    }
    addTransaction(arg1, arg2) {
        if (arg1 instanceof Transaction) {
            this.transactions.push(arg1);
            return arg1.id;
        }
        else if (arg1 && arg2 !== undefined) {
            const transaction = new Transaction(arg1, arg2);
            this.transactions.push(transaction);
            return transaction.id;
        }
        throw new Error('Invalid arguments for addTransaction');
    }
    getTransaction(id) {
        return this.transactions.find((transaction) => transaction.id === id);
    }
    getBalance(currency) {
        return this.transactions
            .filter((transaction) => transaction.currency === currency)
            .reduce((total, transaction) => total + transaction.amount, 0);
    }
}
// Test code
const card = new Card();
// Adding transactions using Transaction object
const transaction1 = new Transaction(100, CurrencyEnum.USD);
const transactionId1 = card.addTransaction(transaction1);
console.log('Transaction Id:', transactionId1);
// Adding transactions using currency and amount
const transactionId2 = card.addTransaction(CurrencyEnum.UAH, 500);
console.log('Transaction Id:', transactionId2);
// Retrieving a transaction by Id
const retrievedTransaction = card.getTransaction(transactionId1);
console.log('Retrieved Transaction:', retrievedTransaction);
// Getting balance by currency
const balanceUSD = card.getBalance(CurrencyEnum.USD);
console.log('Balance (USD):', balanceUSD);
const balanceUAH = card.getBalance(CurrencyEnum.UAH);
console.log('Balance (UAH):', balanceUAH);
