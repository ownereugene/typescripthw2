import { v4 as uuidv4 } from 'uuid'
enum CurrencyEnum {
  USD = "USD",
  UAH = "UAH",
}

interface ICard {
  AddTransaction(transaction: Transaction): void;
  GetTransaction(id: string): Transaction | undefined;
  GetBalance(currency: CurrencyEnum): number;
}

class Transaction {
  public readonly id: string;
  public readonly amount: number;
  public readonly currency: CurrencyEnum;

  constructor(amount: number, currency: CurrencyEnum) {
    this.id = uuidv4();
    this.amount = amount;
    this.currency = currency;
  }
}

class Card implements ICard {
  private transactions: Transaction[] = [];

  public AddTransaction(transaction: Transaction): void {
    this.transactions.push(transaction);
  }

  public GetTransaction(id: string): Transaction | undefined {
    return this.transactions.find((transaction) => transaction.id === id);
  }

  public GetBalance(currency: CurrencyEnum): number {
    return this.transactions
      .filter((transaction) => transaction.currency === currency)
      .reduce((total, transaction) => total + transaction.amount, 0);
  }
}

class BonusCard extends Card {
  public AddTransaction(transaction: Transaction): void {
    super.AddTransaction(transaction);

    const bonusAmount = transaction.amount * 0.1;
    const bonusTransaction = new Transaction(bonusAmount, transaction.currency);
    super.AddTransaction(bonusTransaction);
  }
}

class Pocket {
  private cards: { [name: string]: ICard } = {};

  public AddCard(name: string, card: ICard): void {
    this.cards[name] = card;
  }

  public RemoveCard(name: string): void {
    delete this.cards[name];
  }

  public GetCard(name: string): ICard | undefined {
    return this.cards[name];
  }

  public GetTotalAmount(currency: CurrencyEnum): number {
    let totalAmount = 0;
    for (const cardName in this.cards) {
      const card = this.cards[cardName];
      totalAmount += card.GetBalance(currency);
    }
    return totalAmount;
  }
}

const pocket = new Pocket();

const card1 = new Card();
card1.AddTransaction(new Transaction(100, CurrencyEnum.USD));
card1.AddTransaction(new Transaction(200, CurrencyEnum.UAH));

const card2 = new BonusCard();
card2.AddTransaction(new Transaction(50, CurrencyEnum.USD));
card2.AddTransaction(new Transaction(150, CurrencyEnum.UAH));

pocket.AddCard("Card 1", card1);
pocket.AddCard("Card 2", card2);

console.log(
  "Card 1 balance in USD:",
  pocket.GetCard("Card 1")?.GetBalance(CurrencyEnum.USD)
);
console.log(
  "Card 2 balance in UAH:",
  pocket.GetCard("Card 2")?.GetBalance(CurrencyEnum.UAH)
);
console.log("Total amount in USD:", pocket.GetTotalAmount(CurrencyEnum.USD));
